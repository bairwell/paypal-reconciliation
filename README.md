Paypal_Reconciliation
====================
**Paypal_Reconciliation** takes a Paypal Statement and makes a list of transactions such as:

    07/08/12,414019d959166e3e9,-0.32,17.5,"To/From Paypal: Fee Paid: Person 22 : Item name 22"
    07/08/12,414019d959166e3e9,3.6,21.1,"Shopping cart: Person 22 : Item name 22"

which then can be easily imported into accountancy software such as [Crunch](http://www.crunch.co.uk/?utm_source=PAP&utm_medium=120x60&utm_campaign=symbols&a_aid=bairwell&a_bid=c64918c0).

This module handles currency conversions within Paypal and additional has the functionality to anonymise Paypal statements (removing, in a consistent manner, Paypal transaction ids, people's names and email addresses and invoice item names: amounts/dates are NOT changed).

WARNING - THIS CODE IS CURRENTLY "ALPHA" QUALITY: IT HAS WORKED SUCCESSFULLY WITH *TWO* STATEMENTS I HAVE TESTED IT WITH: HOWEVER, UNIT TESTS ARE CURRENTLY INCOMPLETE AND
IT HAS NOT HAD EXHAUSTIVE TESTING.

This code has been developed by Richard Bairwell of [Bairwell Ltd](http://www.bairwell.com) and is released under the MIT licence.

Simple Usage
============
1. Log into Paypal
2. Go to the "My Account->History->Download History" menu option
3. Enter your date range in the "Custom Date Range" and select "Comma Delimited - All Activity" file type
4. Click "Download History" (do NOT select "Include Shopping cart details").
5. Download the latest copy of the code from BitBucket
 https://bitbucket.org/bairwell/paypal-reconciliation/downloads
6. Extract the Zip file onto your computer (which has PHP 5)
7. From the command line, go into the "paypal-reconciliation" directory"
8. Run the command "php cli_example.php -i "Download.csv" -g -o "Output.csv"
   Where Download.csv is the file you downloaded from Paypal (step 4) and "Output.csv" is the file you want to produce
   Running "php cli_example.php" without any command line parameters will give the options available.
9. Done.

Example Usage for developers
----------------------------

To create a reconciliation file

    /**
     * statement.csv is the downloaded statement from Paypal
     * out.csv is the name of the file we which to output as
     * GBP is the currency the Paypal statement is in
     */
    $input_file       = 'statement.csv';
    $output_file      = 'out.csv';
    $base_currency    = 'GBP';
    /**
     * Extract the data
     */
    $paypal_reconciliation=new \Bairwell\Paypal\Reconciliation(array('input_file'=>$input_file,'base_currency'=>$base_currency));
    $paypal_reconciliation->process();
    /**
     * We want the transactions in a "oldest first" format for Importing
     */
    $outs=$paypal_reconciliation->get_outs()->reverse();
    /**
     * We want to start with the balance as we need a running balance for Importing
     */
    $running=$paypal_reconciliation->get_balance()->get_running();
    $output_handle=fopen($output_file,'w');
    /**
     * @var \Bairwell\Paypal\Reconciliation\Out $out
     */
    foreach ($outs as $out) {
        $running=$running+$out->get_amount();
        $data=array(
            $out->get_date()->format('d/m/y'),
            $out->get_transactionId(),
            $out->get_amount(),
            $running,
            $out->get_description()
        );
        fputcsv($output_handle,$data);
    }
    fclose($output_handle);

To anonymise a Paypal statement:

    $paypal_anonymise=new \Bairwell\Paypal\Anonymise();
    $paypal_anonymise->process('download.csv','anon.csv');

It would be useful if you could submit anonymised files to me to ensure the system correctly processes them in the future.

Copyright
=========
This module was originally written by Richard Bairwell of [Bairwell Ltd](http://www.bairwell.com). Whilst Bairwell Ltd holds the copyright on this work, we have licenced it under the MIT licence.
Bairwell Ltd: [http://www.bairwell.com](http://www.bairwell.com)
Twitter: [http://twitter.com/bairwell](http://www.twitter.com/bairwell)

Richard Bairwell: [http://blog.rac.me.uk](http;//blog.rac.me.uk)
Twitter: [http://twitter.com/rbairwell](http;//twitter.com/rbairwell)

System-Wide Installation
------------------------

ComponentName should be installed using the [PEAR Installer](http://pear.php.net). This installer is the PHP community's de-facto standard for installing PHP components.

    sudo pear channel-discover pear.bairwell.com
    sudo pear install --alldeps pear.bairwell.com/Bairwell_Paypal_Reconciliation

As A Dependency On Your Component
---------------------------------

If you are creating a component that relies on ComponentName, please make sure that you add ComponentName to your component's package.xml file:

```xml
<dependencies>
  <required>
    <package>
      <name>ComponentName</name>
      <channel>pear.bairwell.com</channel>
      <min>0.0.1</min>
      <max>0.0.999</max>
    </package>
  </required>
</dependencies>
```

Usage
-----

The best documentation for Paypal_Reconciliation are the unit tests, which are shipped in the package.  You will find them installed into your PEAR repository, which on Linux systems is normally /usr/share/php/test.

Development Environment
-----------------------

If you want to patch or enhance this component, you will need to create a suitable development environment. The easiest way to do that is to install phix4componentdev:

    # phix4componentdev
    sudo apt-get install php5-xdebug
    sudo apt-get install php5-imagick
    sudo pear channel-discover pear.phix-project.org
    sudo pear -D auto_discover=1 install -Ba phix/phix4componentdev

You can then clone the git repository:

    # Paypal_Reconciliation
    git clone https://bitbucket.org/bairwell/paypal-reconciliation

Then, install a local copy of this component's dependencies to complete the development environment:

    # build vendor/ folder
    phing build-vendor

To make life easier for you, common tasks (such as running unit tests, generating code review analytics, and creating the PEAR package) have been automated using [phing](http://phing.info).  You'll find the automated steps inside the build.xml file that ships with the component.

Run the command 'phing' in the component's top-level folder to see the full list of available automated tasks.

License
-------

See LICENSE.txt for full license details.