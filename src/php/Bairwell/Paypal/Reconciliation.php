<?php
/**
 * Paypal Reconciliation
 *
 * Makes Paypal statements easier to reconcile in services such as Crunch accountancy
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2012 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage Paypal\Reconciliation
 * @author Richard Bairwell
 * @copyright 2012 Bairwell Ltd - http://www.bairwell.com
 * @license MIT
 */
namespace Bairwell\Paypal;

class Reconciliation
{

    /**
     * @var string The input filename
     */
    private $inputFile;

    /**
     * @var string The base currency
     */
    private $baseCurrency;

    /**
     * @var Reconciliation\OutIterator Our Output data
     */
    private $outs;

    /**
     * @var Reconciliation\Balance The current balance
     */
    private $balance;

    public function __construct($arguments = array())
    {
        $this->baseCurrency = 'GBP';
        $this->inputFile = NULL;
        $this->process_arguments($arguments);
    }

    /**
     * Process our arguments
     * @param  array                    $arguments
     * @throws Reconciliation\Exception
     */
    private function process_arguments($arguments)
    {
        if (true === is_array($arguments)) {
            if (true === isset($arguments['inputFile'])) {
                $this->inputFile = $arguments['inputFile'];
            }

            if (true === isset($arguments['baseCurrency'])) {
                $currencies = Reconciliation\Currency::getCurrencies();
                if (false === isset($currencies[$arguments['baseCurrency']])) {
                    throw new Reconciliation\Exception('Unrecognised currency');
                }
                $this->baseCurrency = $arguments['baseCurrency'];
            }
        }
    }

    /**
     * Process the file
     * @throws Reconciliation\Exception
     */
    public function process($arguments = array())
    {
        $this->process_arguments($arguments);
        if (true !== is_string($this->inputFile)) {
            throw new Reconciliation\Exception('Input file is not a string/not set');
        }
        if (true !== file_exists($this->inputFile)) {
            throw new Reconciliation\Exception('Input file does not exist');
        }
        if (true !== is_readable($this->inputFile)) {
            throw new Reconciliation\Exception('Input file is not readable');
        }
        $inputHandle = fopen($this->inputFile, 'r');
        if (false === $inputHandle) {
            throw new Reconciliation\Exception('Unable to open ' . $this->inputFile . ' for reading');
        }
        $in = new Reconciliation\In();
        /**
         * First cache the what needs currency conversions
         */
        $currency = new Reconciliation\Currency($this->baseCurrency);
        $row_count = 0;
        $data = fgetcsv($inputHandle, 2000, ',');
        while (false !== $data) {
            /**
             * Remove spaces either site of the data
             */
            array_walk($data, create_function('&$val', '$val = trim($val);'));
            if ($row_count === 0) {
                $temp = implode(', ', $data);
                if ($temp !== Reconciliation\In::EXPECTED_HEADERS) {
                    throw new Reconciliation\Exception('Invalid header line: got "' . $temp . '" expected "' . Reconciliation\In::EXPECTED_HEADERS . '"');
                }
            } else {
                try {
                    $line = $in->process($data, null);
                } catch (Reconciliation\Exception $e) {
                    throw new Reconciliation\Exception('Reconciliation error on file line ' . $row_count . ':' . $e->getMessage() . ' ' . implode(',', $data));
                } catch (\Exception $e) {
                    throw new Exception('Unexpected error on file line ' . $row_count . ':' . $e->getMessage());
                }
                if (true === $line->getValidTransaction()) {
                    $currency->preprocess($line);
                }
            }
            $row_count++;
            $data = fgetcsv($inputHandle, 2000, ',');
        }
        rewind($inputHandle);
        fgetcsv($inputHandle, 2000, ','); // skip the first line
        /**
         * Now actually extract the data
         */
        $this->outs = new Reconciliation\OutIterator();
        $this->balance = new Reconciliation\Balance();
        $data = fgetcsv($inputHandle, 2000, ',');
        while (false !== $data) {
            /**
             * Remove spaces either site of the data
             */
            array_walk($data, create_function('&$val', '$val = trim($val);'));
            try {
                $line = $in->process($data, $currency);
            } catch (Reconciliation\Exception $e) {
                throw new Reconciliation\Exception(sprintf('In error: %s when processing line: %s', $e->getMessage(), implode(',', $data)));
            } catch (Exception $e) {
                throw new Reconciliation\Exception(sprintf('Unexpected In error: %s when processing line: %s', $e->getMessage(), implode(',', $data)));
            }
            $out = NULL;
            switch ($line->getType()) {
                case 'Cancelled Fee':
                case 'Request Sent':
                case 'Currency Conversion':
                case 'Update to eCheque Sent':
                case 'Invoice Sent';
                    /**
                     * These tend to be "comment items" and can be ignored
                     */
                    break;
                case 'Shopping Cart Item':
                    throw new Reconciliation\Exception('Sorry, can not cope with "Include Shopping Cart details"');
                    break;
                case 'Refund':
                case 'Web Accept Payment Received':
                case 'Shopping Cart Payment Received':
                case 'Donation Received':
                case 'PayPal Express Checkout Payment Received':
                case 'Payment Received':
                case 'Mobile Express Checkout Payment Received':
                case 'Withdraw Funds to a Bank Account':
                case 'Add Funds from a Bank Account':
                case 'Cancelled Transfer':
                case 'Failed Transfer':
                case 'Charge from Credit Card':
                case 'PayPal card confirmation refund':
                case 'Credit to Credit Card':
                case 'Subscription Payment Sent':
                case 'PayPal Express Checkout Payment Sent':
                case 'Payment Sent':
                case 'Shopping Cart Payment Sent':
                case 'Web Accept Payment Sent':
                case 'Mobile Payment Sent':
                case 'Pre-approved Payment Sent':
                case 'Update to Reversal':
                case 'Temporary Hold':
                case 'Payment':
                    /**
                     * "Standard types"
                     */
                    $out = new Reconciliation\Out($line);
                    if (mb_strlen($line->getInvoiceNumber()) > 0) {
                        $out->setDescription(sprintf('%s : %s : %s', $line->getType(), $line->getName(), $line->getInvoiceNumber()));
                    } else {
                        $out->setDescription(sprintf('%s : %s', $line->getType(), $line->getName()));
                    }
                    $out->setAmount($line->getGross());
                    if ($line->getNet() > 0) {
                        $out->setPaymentType(Reconciliation\Out::PAYMENT_TYPE_INBOUND_PAYMENT);
                    } else {
                        $out->setPaymentType(Reconciliation\Out::PAYMENT_TYPE_OUTBOUND_PAYMENT);
                    }
                    $this->outs->push($out);
                    if ($line->getFee() < 0) {
                        $out = new Reconciliation\Out($line);
                        $out->setDescription('To/From Paypal: Fee Paid: ' . $line->getName() . ' : ' . $line->getInvoiceNumber());
                        $out->setAmount($line->getFee());
                        $out->setPaymentType(Reconciliation\Out::PAYMENT_TYPE_FEE_TO_PAYPAL);
                        $this->outs->push($out);
                    } elseif ($line->getFee() > 0) {
                        $out = new Reconciliation\Out($line);
                        $out->setDescription('To/From Paypal: Fee Refunded: ' . $line->getName() . ' : ' . $line->getInvoiceNumber());
                        $out->setAmount($line->getFee());
                        $out->setPaymentType(Reconciliation\Out::PAYMENT_TYPE_FEE_REFUNDED_FROM_PAYPAL);
                        $this->outs->push($out);
                    }
                    $expected = round($line->getFee() + $line->getGross(), 2);
                    if ($expected !== $line->getNet()) {
                        throw new Reconciliation\Exception(
                            sprintf(
                                'Invalid "%s". Net (%0.2f) does not equal expected figure of %0.2f which is gross (%0.2f)+fee (%0.2f) [%s]',
                                $line->getType(), $line->getNet(), $expected, $line->getGross(), $line->getFee(), $line->getTransactionId()
                            )
                        );
                    }
                    $toAdd = 0;
                    $toSub = 0;
                    if ($line->getGross() > 0) {
                        $toAdd += abs($line->getGross());
                    } elseif ($line->getGross() < 0) {
                        $toSub += abs($line->getGross());
                    }
                    if ($line->getFee() > 0) {
                        $toAdd += abs($line->getFee());
                    } elseif ($line->getFee() < 0) {
                        $toSub += abs($line->getFee());
                    }
                    $this->balance->process($line, $toAdd, $toSub);
                    break;
                default:
                    throw new Reconciliation\Exception('Unrecognised transaction type "' . $line->getType() . '" for transaction ' . $line->getTransactionId());
            }
            $data = fgetcsv($inputHandle, 2000, ',');
        }

        fclose($inputHandle);
    }

    /**
     * @return \Bairwell\Paypal\Reconciliation\Balance
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @return \Bairwell\Paypal\Reconciliation\OutIterator
     */
    public function getOuts()
    {
        return $this->outs;
    }

    /**
     * @return string
     */
    public function getBaseCurrency()
    {
        return $this->baseCurrency;
    }

    /**
     * @return string
     */
    public function getInputFile()
    {
        return $this->inputFile;
    }

}
