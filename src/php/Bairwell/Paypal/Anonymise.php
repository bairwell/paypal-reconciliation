<?php
/**
 * Anonymises a Paypal CSV file
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2012 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage Paypal\Reconciliation
 * @author Richard Bairwell
 * @copyright 2012 Bairwell Ltd - http://www.bairwell.com
 * @license MIT
 */
namespace Bairwell\Paypal;

class Anonymise
{

    private $nameMaps;

    private $emailMaps;

    private $transactionIdMaps;

    private $itemTitles;

    /**
     * Process the file
     * @throws Reconciliation\Exception
     */
    public function process($inputFile, $outputFile)
    {
        if (true !== is_string($inputFile)) {
            throw new Reconciliation\Exception('Input file is not a string/not set');
        }
        if (true !== file_exists($inputFile)) {
            throw new Reconciliation\Exception('Input file does not exist');
        }
        if (true !== is_readable($inputFile)) {
            throw new Reconciliation\Exception('Input file is not readable');
        }

        $inputHandle = fopen($inputFile, 'r');
        if (false === $inputHandle) {
            throw new Reconciliation\Exception('Unable to open ' . $inputFile . ' for reading');
        }
        if (null !== $outputFile) {
            $outputHandle = fopen($outputFile, 'w');
            if (false === $outputHandle) {
                throw new Reconciliation\Exception('Unable to open ' . $outputFile . ' for writing');
            }
        }
        /**
         * Anonymise the data
         */
        $row_count = 0;
        $data = fgetcsv($inputHandle, 2000, ',');
        while (false !== $data) {
            /**
             * Remove spaces either site of the data
             */
            array_walk($data, create_function('&$val', '$val = trim($val);'));
            if ($row_count === 0) {
                $temp = implode(', ', $data);
                if ($temp !== Reconciliation\In::EXPECTED_HEADERS) {
                    throw new Reconciliation\Exception('Invalid header line');
                }
            } else {
                for ($col = 0; $col <= Reconciliation\In::COLUMN_BALANCE; $col++) {
                    switch ($col) {
                        case Reconciliation\In::COLUMN_NAME:
                            /** Names */
                            if (strlen($data[$col]) > 0) {
                                $hash = md5($data[$col]);
                                if (isset($this->nameMaps[$hash]) === FALSE) {
                                    $this->nameMaps[$hash] = 'Person ' . (count($this->nameMaps) + 1);
                                }
                                $data[$col] = $this->nameMaps[$hash];
                            }
                            break;
                        case Reconciliation\In::COLUMN_FROMEMAILADDRESS:
                        case Reconciliation\In::COLUMN_TOEMAILADDRESS:
                            /**
                             * Email addresses
                             */
                            if (strlen($data[$col]) > 0) {
                                $hash = md5($data[$col]);
                                if (isset($this->emailMaps[$hash]) === FALSE) {
                                    $this->emailMaps[$hash] = 'email_' . (count($this->emailMaps) + 1) . '@example.com';
                                }
                                $data[$col] = $this->emailMaps[$hash];
                            }
                            break;
                        case Reconciliation\In::COLUMN_TRANSACTIONID:
                        case Reconciliation\In::COLUMN_REFERENCETRANSACTIONID:
                            /**
                             * Transaction ids
                             */
                            if (strlen($data[$col]) > 0) {
                                $hash = md5($data[$col]);
                                if (isset($this->transactionIdMaps[$hash]) === FALSE) {
                                    $this->transactionIdMaps[$hash] = strtoupper(substr($hash . $hash . $hash, 0, strlen($data[$col])));
                                }
                                $data[$col] = $this->transactionIdMaps[$hash];
                            }
                            break;
                        case Reconciliation\In::COLUMN_INVOICENUMBER:
                        case Reconciliation\In::COLUMN_ITEMTITLE:
                            /**
                             * Item/invoice titles
                             */
                            if (strlen($data[$col]) > 0) {
                                $hash = md5($data[$col]);
                                if (isset($this->itemTitles[$hash]) === FALSE) {
                                    $this->itemTitles[$hash] = 'Item name ' . (count($this->itemTitles) + 1);
                                }
                                $data[$col] = $this->itemTitles[$hash];
                            }
                            break;
                    }
                }
            }
            if (null === $outputFile) {
                print implode(',', $data) . PHP_EOL;
            } else {
                fputcsv($outputHandle, $data);
            }
            $data = fgetcsv($inputHandle, 2000, ',');
            $row_count++;
        }
        fclose($inputHandle);
        if (null !== $outputFile) {
            fclose($outputHandle);
        }
    }
}
