<?php
/**
 * Iterator for multiple Paypal output lines
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2012 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage Paypal\Reconciliation
 * @author Richard Bairwell
 * @copyright 2012 Bairwell Ltd - http://www.bairwell.com
 * @license MIT
 */
namespace Bairwell\Paypal\Reconciliation;

/**
 * Processes multiple Paypal output lines
 */
class OutIterator implements \Iterator
{

    /**
     * @var int What position do we start in
     */
    private $position = 0;

    /**
     * @var array Our collection of "Out" data
     */
    private $data;

    /**
     * @var array Keep track of our unique ids
     */
    private $uniqueids;

    /**
     * Constructor
     * @param array $data Any data we should start with
     */
    public function __construct($data = array())
    {
        $this->position = 0;
        $this->uniqueids=array();
        if (empty($data) === FALSE) {
            foreach ($data as $line) {
                $this->push($line);
            }
        }
    }

    /**
     * Push an item onto the stack
     * @param  Out       $line
     * @throws Exception
     */
    public function push($line)
    {
        if (FALSE===($line instanceof Out)) {
            throw new Exception('Can only have "Out" pushed onto this stack');
        }
        if (TRUE===isset($this->uniqueids[$line->getUniqueid()])) {
            throw new Exception('This "Out" has already been pushed onto the stack');
        }
        $this->data[] = $line;
    }

    /**
     * Reverse the order of the data (handy for outputting)
     * @return OutIterator
     */
    public function reverse()
    {
        return new OutIterator(array_reverse($this->data));
    }

    /**
     * Rewind
     */
    public function rewind()
    {
        $this->position = 0;
    }

    /**
     * Get current position
     * @return mixed
     */
    public function current()
    {
        return $this->data[$this->position];
    }

    /**
     * Get Key
     * @return int
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * Get next position
     */
    public function next()
    {
        ++$this->position;
    }

    /**
     * Is this a valid entry
     * @return bool
     */
    public function valid()
    {
        return isset($this->data[$this->position]);
    }

    public function getLast($num=1) {
        $out=array();
        $count=count($this->data);
        if ($count>=$num) {
            for ($i=1;$i<=$num;$i++) {
                $out[]=$this->data[$count-$i];
            }
        } else {
            throw new Exception(sprintf('Number of rows (%d) requested exceeds data buffer (%d)',$num,$count));
        }
        return $out;
    }
}
