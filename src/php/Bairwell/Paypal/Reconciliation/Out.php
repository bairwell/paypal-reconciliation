<?php
/**
 * Handle a Paypal output line
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2012 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage Paypal\Reconciliation
 * @author Richard Bairwell
 * @copyright 2012 Bairwell Ltd - http://www.bairwell.com
 * @license MIT
 */
namespace Bairwell\Paypal\Reconciliation;

/**
 * Handles a Paypal output line
 *
 */
class Out
{

    const PAYMENT_TYPE_UNKNOWN = 0;
    const PAYMENT_TYPE_INBOUND_PAYMENT = 1;
    const PAYMENT_TYPE_OUTBOUND_PAYMENT = 2;
    const PAYMENT_TYPE_FEE_TO_PAYPAL = 3;
    const PAYMENT_TYPE_FEE_REFUNDED_FROM_PAYPAL = 4;
    const PAYMENT_TYPE_WITHDRAWAL = 5;
    const PAYMENT_TYPE_CREDIT = 6;

    /**
     * @var \DateTime The date/time of this entry
     */
    private $date;
    /**
     * @var float The amount of this entry
     */
    private $amount;
    /**
     * @var string The description of this entry
     */
    private $description;

    /**
     * @var string The Paypal Transaction Id
     */
    private $transactionId;

    /**
     * @var int The payment type constant
     */
    private $paymentType;

    /**
     * @var string This internal tracking id (to prevent duplicates)
     */
    private $uniqueid;

    /**
     * Constructor
     * @param ConvertedLine $data
     */
    public function __construct(ConvertedLine $data)
    {
        $this->date = $data->getDatetime();
        $this->transactionId = $data->getTransactionId();
        $this->uniqueid=uniqid(rand(1000,9999),true);
    }

    /**
     * Sets amount in a fluent manner
     *
     * @param float $amount
     *
     * @return Out
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get the amount of this line
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Get the transaction id
     * @return string
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * Get the date/time
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Sets description in a fluent manner
     *
     * @param string $description
     *
     * @return Out
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Returns the description
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param int $paymentType
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;
    }

    /**
     * @return int
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * @return string
     */
    public function getUniqueid()
    {
        return $this->uniqueid;
    }


}
