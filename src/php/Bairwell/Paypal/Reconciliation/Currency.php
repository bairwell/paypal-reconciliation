<?php
/**
 * Handle a Paypal currency
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2012 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage Paypal\Reconciliation
 * @author Richard Bairwell
 * @copyright 2012 Bairwell Ltd - http://www.bairwell.com
 * @license MIT
 */
namespace Bairwell\Paypal\Reconciliation;

/**
 * Handle a Paypal Currency conversions
 */
class Currency
{

    private $baseCurrency;

    private $references;

    public function __construct($baseCurrency)
    {
        $this->baseCurrency = $baseCurrency;
    }

    public function preprocess(ConvertedLine $line)
    {
        if ($line->getCurrency() !== $this->baseCurrency || $line->getType() === 'Currency Conversion') {
            $trackingReference = $line->getDate() . $line->getTime();
            if (false === isset($this->references[$trackingReference]) || false === is_array($this->references[$trackingReference])) {
                $this->references[$trackingReference] = array();
            }
            $this->references[$trackingReference][] = $line;
        }
    }

    /**
     * Returns the original line untouched, "null" if the line should be skipped or an amended line
     * @param ConvertedLine $line
     * @throws
     */
    public function process(ConvertedLine $line)
    {
        if ($line->getType() === 'Currency Conversion') {
            return null;
        }
        if ($line->getCurrency() !== $this->baseCurrency) {
            $trackingReference = $line->getDate() . $line->getTime();
            if (false === isset($this->references[$trackingReference]) || false === is_array($this->references[$trackingReference])) {
                throw Exception('Unrecognised currency conversion line');
            }
            // Okay, let's do the conversion
            $references = $this->references[$trackingReference];
            $currencies = self::getCurrencies();
            $original = null;
            $native = null;
            $foreign = null;
            if (count($references) > 3) {
                throw new Exception('Too many matching transactions at the same date/time');
            }
            /**
             * @var $checkLine ConvertedLine
             */
            foreach ($references as $checkLine) {
                if ($checkLine->getType() === 'Currency Conversion') {
                    if ($this->baseCurrency===$checkLine->getCurrency()) {
                        $native=$checkLine;
                    } else {
                        $foreign=$checkLine;
                    }
                } elseif ($checkLine->getCurrency() === $line->getCurrency() && $checkLine->getTransactionId() === $line->getTransactionId()) {
                    $original = $checkLine;
                } else {
                    throw new Exception('Unrecognised cached currency conversion line');
                }
            }
            if (null === $native) {
                throw new Exception('Unable to find native currency');
            } elseif (null === $foreign) {
                throw new Exception('Unable to find foreign currency');
            } elseif (null === $original) {
                throw new Exception('Unable to find original transaction records');
            }
            if ($native->getName()!=='To '.$currencies[$line->getCurrency()] && $native->getName()!=='From '.$currencies[$line->getCurrency()] ) {
                throw new Exception(
                    sprintf(
                        'Transaction %s: Currency conversion failed as native transaction name (%s) does not match that generated from the original transaction (currency %s) which is (%s)',
                        $line->getTransactionId(),
                        $native->getName(),
                        $line->getCurrency(),
                       $currencies[$line->getCurrency()]
                            )
                );
            } elseif ($foreign->getName()!=='To '.$currencies[$this->baseCurrency] && $foreign->getName()!=='From '.$currencies[$this->baseCurrency] ) {
                throw new Exception(
                    sprintf(
                        'Transaction %s: Currency conversion failed as foreign transaction name (%s) does not match that generated from the native transaction (currency %s) which is (%s)',
                        $line->getTransactionId(),
                        $foreign->getName(),
                        $this->baseCurrency,
                        $currencies[$this->baseCurrency]
                    )
                );
            }
            /**
             * Okay, the data looks sane : let's overwrite it, preserving the negative/postive settings of the original amount (for refunds et al)
             */
            if ($line->getFee()>0) {
                $line->setFee(abs($native->getFee()));
            } else {
                $line->setFee(0-abs($native->getFee()));
            }
            if ($line->getGross()>0) {
                $line->setGross(abs($native->getGross()));
            } else {
                $line->setGross(0-abs($native->getGross()));
            }
            if ($line->getNet()>0) {
                $line->setNet(abs($native->getNet()));
            } else {
                $line->setNet(0-abs($native->getNet()));
            }
            $line->setBalance($native->getBalance());
            $line->setCurrency( $native->getCurrency());
        }

        return $line;
    }

    public static function getCurrencies()
    {
        return array(
            'AUD' => 'Australian Dollar',
            'BRL' => 'Brazilian Real',
            'CAD' => 'Canadian Dollar',
            'CZK' => 'Czech Koruna',
            'DKK' => 'Danish Krone',
            'EUR' => 'Euro',
            'HKD' => 'Hong Kong Dollar',
            'HUF' => 'Hungarian Forint',
            'ILS' => 'Israeli New Shekel',
            'JPY' => 'Japanese Yen',
            'MYR' => 'Malaysian Ringgit',
            'MXN' => 'Mexican Peso',
            'NOK' => 'Norwegian Krone',
            'NZD' => 'New Zealand Dollar',
            'PHP' => 'Philippine Peso',
            'PLN' => 'Polish Zloty',
            'GBP' => 'British Pound',
            'SGD' => 'Singapore Dollar',
            'SEK' => 'Swedish Krona',
            'CHF' => 'Swiss Franc',
            'TWD' => 'New Taiwan Dollar',
            'THB' => 'Thai Baht',
            'TRY' => 'Turkish Lira',
            'USD' => 'US Dollar'
        );
    }

    /**
     * Return the number of decimal points a currency code has
     * @param  string $currency
     * @return int
     */
    public static function getCurrencyDecimalPoints($currency)
    {
        if (true === in_array($currency, array('JPY', 'TWD', 'TRY'))) {
            return 0;
        } else {
            return 2;
        }
    }

}
