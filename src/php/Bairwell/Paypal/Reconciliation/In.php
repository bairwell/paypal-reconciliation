<?php
/**
 * Handle a Paypal input line
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2012 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage Paypal\Reconciliation
 * @author Richard Bairwell
 * @copyright 2012 Bairwell Ltd - http://www.bairwell.com
 * @license MIT
 */
namespace Bairwell\Paypal\Reconciliation;

/**
 * Handle a Paypal CSV line
 */
class In
{
    /**
     * @var string The headers of the file we are expecting
     */
    const EXPECTED_HEADERS = 'Date, Time, Time Zone, Name, Type, Status, Currency, Gross, Fee, Net, From Email Address, To Email Address, Transaction ID, Address Status, Item Title, Item ID, Reference Txn ID, Invoice Number, Receipt ID, Balance, ';

    const COLUMN_DATE = 0;
    const COLUMN_TIME = 1;
    const COLUMN_TIMEZONE = 2;
    const COLUMN_NAME = 3;
    const COLUMN_TYPE = 4;
    const COLUMN_STATUS = 5;
    const COLUMNCurrency = 6;
    const COLUMN_GROSS = 7;
    const COLUMN_FEE = 8;
    const COLUMN_NET = 9;
    const COLUMN_FROMEMAILADDRESS = 10;
    const COLUMN_TOEMAILADDRESS = 11;
    const COLUMN_TRANSACTIONID = 12;
    const COLUMN_ADDRESSSTATUS = 13;
    const COLUMN_ITEMTITLE = 14;
    const COLUMN_ITEMID = 15;
    const COLUMN_REFERENCETRANSACTIONID = 16;
    const COLUMN_INVOICENUMBER = 17;
    const COLUMN_RECEIPTID = 18;
    const COLUMN_BALANCE = 19;

    /**
     * Process
     * @param  array       $data     The array data from the CSV file
     * @param  object|null $currency Any currency conversion data we should use
     * @throws Exception   If anything is deemed as invalid
     **/
    public function process($data, Currency $currency = null)
    {
        $line = new ConvertedLine();
        $line->setType($data[self::COLUMN_TYPE])
            ->setTransactionId($data[self::COLUMN_TRANSACTIONID])
            ->setDate($data[self::COLUMN_DATE])
            ->setTime($data[self::COLUMN_TIME])
            ->setTimezone($data[self::COLUMN_TIMEZONE])
            ->setName($data[self::COLUMN_NAME])
            ->setType($data[self::COLUMN_TYPE])
            ->setStatus($data[self::COLUMN_STATUS])
            ->setCurrency($data[self::COLUMNCurrency])
            ->setGross($data[self::COLUMN_GROSS])
            ->setFee($data[self::COLUMN_FEE])
            ->setNet($data[self::COLUMN_NET])
            ->setFromEmail($data[self::COLUMN_FROMEMAILADDRESS])
            ->setRelatedTransaction($data[self::COLUMN_REFERENCETRANSACTIONID])
            ->setInvoiceNumber($data[self::COLUMN_INVOICENUMBER])
            ->setValidTransaction(TRUE);

        /**
         * Is this a transaction we should ignore (i.e. it isn't actually balance affecting)?
         */
        if (true === in_array($line->getType(), array('Invoice Sent', 'Request Sent', 'Update to eCheque Sent'))) {
            $line->setValidTransaction(FALSE);
        } else {
            $line->setBalance($data[self::COLUMN_BALANCE]);
            if (null !== $currency) {
                $convert = $currency->process($line);
                if (null === $convert) {
                    $line->setValidTransaction(FALSE);
                } else {
                    $line = $convert;
                }
            }
        }
        return $line;
    }

}
