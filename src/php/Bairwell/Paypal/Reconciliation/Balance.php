<?php
/**
 * Handle Paypal Balances
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2012 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage Paypal\Reconciliation
 * @author Richard Bairwell
 * @copyright 2012 Bairwell Ltd - http://www.bairwell.com
 * @license MIT
 */
namespace Bairwell\Paypal\Reconciliation;

/**
 * Handles Paypal balances
 */
class Balance
{

    /**
     * @var float Current running total
     */
    private $running;

    /**
     * @var string What we last did
     */
    private $lastTransactionActions;

    /**
     * @var bool Whether we should verify the amounts
     */
    private $verify;

    /**
     * @var string List of all actions we've taken
     */
    private $all_actions;

    /**
     * Get the current running balance to two decimal places
     * @return float
     */
    public function getRunning()
    {
        return round($this->running, 2);
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->running = NULL;
        $this->verify = FALSE;
    }

    /**
     * @param ConvertedLine    $line     The line we are currently processing in Paypal
     * @param float $add      How much are we adding?
     * @param float $subtract How much are we subtracting?
     *
     * @throws Exception
     */
    public function process(ConvertedLine $line, $add, $subtract)
    {
        $this->all_actions .= PHP_EOL . $this->lastTransactionActions;
        $actions = '';
        /**
         * We only want to verify the running total if this isn't the first valid
         * line (i.e. we've set Verify to true) and if there is no related transaction id
         * (this helps avoid scenerios such as refunds coming from bank accounts)
         */
        if ($this->verify === TRUE && '' === $line->getRelatedTransaction()) {
            if (round($line->getBalance(), 2) !== round($this->running, 2)) {
                throw new Exception(sprintf('Balance: Running total of %0.2f does not match line balance %0.2f [%s] after %s',
                    $this->running, $line->getBalance() ,$line->getTransactionId(), $this->all_actions));
            }
        }
        $original_running = $this->running;
        if ($this->running === NULL) {
            $original_running = '[null]';
            $this->running = $line->getBalance();
            $actions = sprintf('Set balance to %0.2f. ',$this->running);
            $this->verify = TRUE;
        }
        /**
         * Reverse them as we are working backwards
         */
        $temp = abs($add);
        $add = abs($subtract);
        $subtract = $temp;
        if ($add > 0) {
            $actions .= sprintf('Adding %0.2f ',$add);
            $this->running += $add;
        }
        if ($subtract > 0) {
            $actions .= sprintf('Subtracting %0.2f ',$subtract);
            $this->running -= $subtract;
        }
        $this->lastTransactionActions = sprintf('Processed "%s" (%s) [%s %s] to change %0.2f to %0.2f (by %s). Current: %0.2f',
            $line->getType(),$line->getStatus(),$line->getTransactionId() ,$line->getName(), $original_running,  $this->running,$actions,$this->running);
    }
}
