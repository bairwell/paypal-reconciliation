<?php
namespace Bairwell\Paypal\Reconciliation;

class ConvertedLine
{
    /**
     * @var string The date as sent by Paypal
     */
    private $date;
    /**
     * @var string The time as sent by Paypal
     */
    private $time;
    /**
     * @var string The timezone as sent by Payapl
     */
    private $timezone;
    /**
     * @var string The name of the payee/receiver
     */
    private $name;
    /**
     * @var string The type of the transaction
     */
    private $type;
    /**
     * @var string Status of the transaction
     */
    private $status;
    /**
     * @var string Currency of this transaction
     */
    private $currency;
    /**
     * @var float Gross amount (i.e. fee+net)
     */
    private $gross;
    /**
     * @var float The Paypal fee they've deducted/refunded
     */
    private $fee;
    /**
     * @var float The net amount paid
     */
    private $net;
    /**
     * @var string Who was it sent from
     */
    private $fromEmail;
    /**
     * @var string The transaction id
     */
    private $transactionId;
    /**
     * @var string Any related transaction ids (useful for currency conversions and refunds)
     */
    private $relatedTransaction;
    /**
     * @var string Any invoice numbers/strings
     */
    private $invoiceNumber;
    /**
     * @var float The current balance in this currency
     */
    private $balance;
    /**
     * @var string Notes about any currency conversions
     */
    private $conversionNotes;

    /**
     * @var bool True if this is a valid transaction, false if it's just a "comment line"
     */
    private $validTransaction;

    /**
     * @param float $balance
     */
    public function setBalance($balance)
    {
        $regexp = '/^\-?[0-9]{1,}';
        $dp = Currency::getCurrencyDecimalPoints($this->currency);
        if ($dp > 0) {
            $regexp .= '(\.[0-9]' . str_repeat('[0-9]?', ($dp-1)) . ')?';
        }
        $regexp .= '$/';
        if (preg_match($regexp, $balance) !== 1) {
            throw new Exception('Invalid balance figure: ' . $balance);
        }
        $this->balance = $balance;

        return $this;
    }

    /**
     * @param string $conversionNotes
     */
    public function setConversionNotes($conversionNotes)
    {
        $this->conversionNotes = $conversionNotes;

        return $this;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $currencies = Currency::getCurrencies();
        if (false === isset($currencies[$currency])) {
            throw new Exception('Invalid currency: ' . $currency);
        }
        $this->currency = $currency;

        return $this;
    }

    /**
     * @param string $date
     */
    public function setDate($date)
    {
        if (preg_match('/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/(199[0-9]|20[0-2][0-9])$/', $date) !== 1) {
            throw new Exception('Invalid date: ' . $date);
        }
        $this->date = $date;

        return $this;
    }

    /**
     * @param float $fee
     */
    public function setFee($fee)
    {
        if (true === is_string($fee)) {
            if ('...'===$fee) {
                $fee='0.00';
            } else {
            $regexp = '/^\-?[0-9]{1,}';
            $dp = Currency::getCurrencyDecimalPoints($this->currency);
            if ($dp > 0) {
                $regexp .= '(\.[0-9]' . str_repeat('[0-9]?', ($dp-1)) . ')?';
            }
            $regexp .= '$/';
            if (preg_match($regexp, $fee) !== 1) {
                throw new Exception(sprintf('Invalid fee figure (%s) found', $fee));
            }
            }
        }
        $this->fee = (float)$fee;

        return $this;
    }

    /**
     * @param string $fromEmail
     */
    public function setFromEmail($fromEmail)
    {
        $this->fromEmail = $fromEmail;

        return $this;
    }

    /**
     * @param float $gross
     */
    public function setGross($gross)
    {
        if (true === is_string($gross)) {
            $regexp = '/^\-?[0-9]{1,}';
            $dp = Currency::getCurrencyDecimalPoints($this->currency);
            if ($dp > 0) {
                $regexp .= '(\.[0-9]' . str_repeat('[0-9]?', ($dp-1)) . ')?';
            }
            $regexp .= '$/';
            if (preg_match($regexp, $gross) !== 1) {
                throw new Exception(sprintf('Invalid gross figure (%s) found', $gross));
            }
        }
        $this->gross = (float)$gross;

        return $this;
    }

    /**
     * @param string $invoiceNumber
     */
    public function setInvoiceNumber($invoiceNumber)
    {
        $this->invoiceNumber = $invoiceNumber;

        return $this;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param float $net
     */
    public function setNet($net)
    {
        if (true === is_string($net)) {
            $regexp = '/^\-?[0-9]{1,}';
            $dp = Currency::getCurrencyDecimalPoints($this->currency);
            if ($dp > 0) {
                $regexp .= '(\.[0-9]' . str_repeat('[0-9]?', ($dp-1)) . ')?';
            }
            $regexp .= '$/';
            if (preg_match($regexp, $net) !== 1) {
                throw new Exception(sprintf('Invalid net figure (%s) found', $net));
            }
        }
        $this->net = (float)$net;

        return $this;
    }

    /**
     * @param string $relatedTransaction
     */
    public function setRelatedTransaction($relatedTransaction)
    {
        $this->relatedTransaction = $relatedTransaction;

        return $this;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @param string $time
     */
    public function setTime($time)
    {
        if (preg_match('/^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/', $time) !== 1) {
            throw new Exception('Invalid time: ' . $time);
        }
        $this->time = $time;

        return $this;
    }

    /**
     * @param string $timezone
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * @param string $transactionId
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;

        return $this;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @param boolean $validTransaction
     */
    public function setValidTransaction($validTransaction)
    {
        $this->validTransaction = $validTransaction;

        return $this;
    }

    /**
     * Gets the date/time of this transaction
     * @return \DateTime
     * @throws Exception If unable to create a PHP \DateTime object from Paypal data
     */
    public function getDatetime()
    {
        $time = \DateTime::createFromFormat('d/m/Y G:i:s', $this->date . ' ' . $this->time);
        if ($time === FALSE) {
            throw new Exception('Unable to decode time ' . $this->date . ' ' . $this->time . ': ' . print_r(\DateTime::getLastErrors(), TRUE));
        }
        $time->setTimezone(new \DateTimeZone($this->timezone));

        return $time;
    }

    /**
     * Gets the related transaction id
     * @return string
     */
    public function getRelatedTransaction()
    {
        return $this->relatedTransaction;
    }

    /**
     * Gets the transaction id
     * @return string
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * Gets the balance
     * @return float
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Gets the currency
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Gets the date
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Gets the fee
     * @return float
     */
    public function getFee()
    {
        return round($this->fee, 2);
    }

    /**
     * Gets the from email
     * @return string
     */
    public function getFromEmail()
    {
        return $this->fromEmail;
    }

    /**
     * Gets the invoice number
     * @return string
     */
    public function getInvoiceNumber()
    {
        return $this->invoiceNumber;
    }

    /**
     * Gets the gross figure
     * @return float
     */
    public function getGross()
    {
        return round($this->gross, 2);
    }

    /**
     * Gets the name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Gets the net amount
     * @return float
     */
    public function getNet()
    {
        return round($this->net, 2);
    }

    /**
     * Gets the transaction status
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Gets the time
     * @return string
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Gets the timezone
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Gets the transaction type
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Gets whether or not this is a valid transaction
     * @return boolean
     */
    public function getValidTransaction()
    {
        return $this->validTransaction;
    }

}
