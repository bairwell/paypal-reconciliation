<?php
if (defined('PHP_MAJOR_VERSION') === FALSE) {
    die('We require PHP 5.3.0 at a minimum as we use namespaces and late static bindings. You have PHP ' . phpversion());
}
if (PHP_MAJOR_VERSION === 5 && PHP_MINOR_VERSION < 3) {
    die('We require PHP 5.3.0 at a minimum as we use namespaces and late static bindings. You have PHP ' . phpversion());
}
/**
 * We use UTF-8 encoding
 */
mb_internal_encoding('UTF-8');

/**
 * Load the autoloader
 */
$dirname = dirname(__FILE__);
include 'Bairwell/Autoloader.php';
$autoloader = new \Bairwell\Autoloader();
$autoloader->setDebugState(TRUE);
spl_autoload_register(array($autoloader, 'load'));
$autoloader->addPrefix('Bairwell', $dirname . '/../../php/Bairwell/');

$input_file = '/LinuxFiles/Downloads/Download.csv';
$output_file = 'out.csv';
$base_currency = 'GBP';
$paypal_reconciliation = new Bairwell\Paypal\Reconciliation(array('input_file' => $input_file, 'base_currency' => $base_currency));
$paypal_reconciliation->process();

$outs = $paypal_reconciliation->get_outs()->reverse();

$running = $paypal_reconciliation->getBalance()->getRunning();

$outputHandle = fopen($output_file, 'w');

$totalFees = 0;
$latestDate = '';
/**
 * @var Bairwell\Paypal\Reconciliation\Out $out
 */
foreach ($outs as $out) {

    if ($out->get_paymentType() === \Bairwell\Paypal\Reconciliation\Out::PAYMENT_TYPE_FEE_TO_PAYPAL) {
        $totalFees += abs($out->get_amount());
    } elseif ($out->get_paymentType() === \Bairwell\Paypal\Reconciliation\Out::PAYMENT_TYPE_FEE_TO_PAYPAL) {
        $totalFees -= abs($out->get_amount());
    } else {
        $running = $running + $out->get_amount();
        $latestDate = $out->getDate()->format('d/m/y');
        $data = array(
            $out->getDate()->format('d/m/y'),
            $out->getTransactionId(),
            sprintf('%0.2f', $out->get_amount()),
            sprintf('%0.2f', $running),
            $out->getDescription()
        );
        fputcsv($outputHandle, $data);
    }
}
$data = array($latestDate, 'Paypal Fees Summary', sprintf('%0.2f', 0 - $totalFees), sprintf('%0.2f', $running - $totalFees), 'Summarised Paypal Fees');
fputcsv($outputHandle, $data);
fclose($outputHandle);
print 'Total fees: ' . $totalFees . PHP_EOL;

//$paypal_anonymise=new Bairwell\Paypal\Anonymise();
//$paypal_anonymise->process('download.csv','anon.csv');
