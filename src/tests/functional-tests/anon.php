<?php
/**
 * Anonymises a download.csv file
 */
if (defined('PHP_MAJOR_VERSION') === FALSE) {
    die('We require PHP 5.3.0 at a minimum as we use namespaces and late static bindings. You have PHP ' . phpversion());
}
if (PHP_MAJOR_VERSION === 5 && PHP_MINOR_VERSION < 3) {
    die('We require PHP 5.3.0 at a minimum as we use namespaces and late static bindings. You have PHP ' . phpversion());
}
/**
 * We use UTF-8 encoding
 */
mb_internal_encoding('UTF-8');

/**
 * Load the autoloader
 */
$dirname = dirname(__FILE__);
include 'Bairwell/Autoloader.php';
$autoloader = new \Bairwell\Autoloader();
$autoloader->setDebugState(TRUE);
spl_autoload_register(array($autoloader, 'load'));
$autoloader->addPrefix('Bairwell', $dirname . '/../../php/Bairwell/');

$input_file = 'anon.csv';
$output_file = 'out.csv';

$paypal_anonymise = new Bairwell\Paypal\Anonymise();
$paypal_anonymise->process('download.csv', 'anon.csv');
