<?php
if (defined('PHP_MAJOR_VERSION') === FALSE) {
    die('We require PHP 5.3.0 at a minimum as we use namespaces and late static bindings. You have PHP ' . phpversion());
}
if (PHP_MAJOR_VERSION === 5 && PHP_MINOR_VERSION < 3) {
    die('We require PHP 5.3.0 at a minimum as we use namespaces and late static bindings. You have PHP ' . phpversion());
}
/**
 * We use UTF-8 encoding
 */
mb_internal_encoding('UTF-8');

/**
 * Load the autoloader
 */
$dirname = dirname(__FILE__);
include 'Autoloader.php';
$autoloader = new \Bairwell\Autoloader();
$autoloader->setDebugState(TRUE);
spl_autoload_register(array($autoloader, 'load'));
$autoloader->addPrefix('Bairwell', $dirname . '/src/php/Bairwell/');

if (true === isset($_SERVER['argc'])) {
    run();
} else {
    print 'This is a command line PHP script.';
    exit(0);
}

function run()
{
    $clioptions = getopt('i:o:gc::ha', array('input', 'output', 'group', 'currency', 'help', 'anon'));
    if (false === is_array($clioptions)) {
        print 'There was a problem reading in the CLI options.';
        die();
    }
    $input = mergeOptions($clioptions, 'i', 'input');
    $output = mergeOptions($clioptions, 'o', 'output');
    $currency = mb_strtoupper(trim(mergeOptions($clioptions, 'c', 'currency')));
    $help = mergeOptions($clioptions, 'h', 'help');
    $group = mergeOptions($clioptions, 'g', 'group');
    $anon = mergeOptions($clioptions, 'a', 'anon');
    if (null !== $help) {
        displayHelp();

        return;
    }
    /**
     * Validate the currency
     */
    if ((null !== $currency || mb_strlen($currency) < 1)) {
        $currency = 'GBP';
    }
    $currencies = \Bairwell\Paypal\Reconciliation\Currency::getCurrencies();
    if (false === isset($currencies[$currency])) {
        print 'Sorry, the "base currency" needs to be one of the supported ones:' . PHP_EOL;
        print implode(',', array_keys($currencies));
        displayHelp();

        return;
    }
    /**
     * Validate the input file
     */
    if (false === is_string($input) || strlen($input) < 1) {
        print 'No input filename specified' . PHP_EOL;
        displayHelp();

        return;
    }
    $input = realpath($input);
    if (true !== file_exists($input)) {
        print $input . ' input file does not exist' . PHP_EOL;
        die();
    }
    if (true !== is_readable($input)) {
        print $input . ' input file is not readable by ' . get_current_user() . PHP_EOL;
        die();
    }
    if (true !== is_file($input)) {
        print $input . ' is not a file' . PHP_EOL;
        die();
    }
    /**
     * Validate the output file
     */
    if (false === is_string($output) || strlen($output) < 1) {
        $output = null;
    } else {
        if (true === file_exists($output)) {
            print $output . ' already exists for output' . PHP_EOL;
            if (true !== is_writable($output)) {
                print $output . ' cannot be overwritten or modified for output' . PHP_EOL;
                die();
            }
            print 'Are you sure you wish to continue? (y/N)?' . PHP_EOL;
            $in = fopen("php://stdin", "r");
            $inString = fgets($in, 10);
            fclose($in);
            if ('Y' !== mb_substr(mb_strtoupper(trim($inString)), 0, 1)) {
                return;
            } else {
                print 'continuing...' . PHP_EOL;
            }
        }
    }
    $groupFees = false;
    if (null !== $group) {
        $groupFees = true;
    }
    /**
     * Show what we are doing
     */
    print str_repeat('=', 30) . PHP_EOL;
    print 'Processing input file :' . $input . PHP_EOL;
    if (null === $output) {
        print 'Output to be sent to screen' . PHP_EOL;
    } else {
        print 'Sending to output file:' . $output . PHP_EOL;
    }
    print 'Using base currency   :' . $currency . PHP_EOL;
    try {
        if (null === $anon) {
            if (true === $groupFees) {
                print 'Grouping Paypal fees' . PHP_EOL;
            }
            print 'Processing file....' . PHP_EOL;
            print str_repeat('=', 30) . PHP_EOL;
            process($input, $output, $currency, $groupFees);
        } else {
            print 'Anonymising Paypal file' . PHP_EOL;
            print str_repeat('=', 30) . PHP_EOL;
            $reconciliation = new \Bairwell\Paypal\Anonymise();
            $reconciliation->process($input, $output);
        }
    } catch (\Bairwell\Paypal\Reconciliation\Exception $e) {
        print 'Problem during Reconciliation:' . PHP_EOL;
        die($e->getMessage());
    } catch (\Exception $e) {
        die($e->getMessage());
    }
    print 'Finished' . PHP_EOL;
}

function process($inputFile, $outputFile, $baseCurrency = 'GBP', $group = FALSE)
{
    /**
     * Process the file
     */
    $reconciliation = new \Bairwell\Paypal\Reconciliation(array('inputFile' => $inputFile, 'baseCurrency' => $baseCurrency));
    $reconciliation->process();

    /**
     * Reverse the listing so we have oldest first
     */
    $outs = $reconciliation->getOuts()->reverse();

    /**
     * Get the starting running balance
     */
    $running = $reconciliation->getBalance()->getRunning();
    if (null !== $outputFile) {
        $outputHandle = fopen($outputFile, 'w');
    }
    $header = array('Date', 'Transaction id', 'Amount', 'Balance', 'Description');
    if (null === $outputFile) {
        print implode(',', $header) . PHP_EOL;
    } else {
        fputcsv($outputHandle, $header);
    }
    $totalFees = 0;
    $latestDate = '';
    $firstDate=NULL;
    /**
     * @var \Bairwell\Paypal\Reconciliation\Out $out
     */
    foreach ($outs as $out) {
        if (NULL===$firstDate) {
            $firstDate= $out->getDate()->format('d/m/y');
        }
        if (true === $group && ($out->getPaymentType() === \Bairwell\Paypal\Reconciliation\Out::PAYMENT_TYPE_FEE_TO_PAYPAL || $out->getPaymentType() === \Bairwell\Paypal\Reconciliation\Out::PAYMENT_TYPE_FEE_REFUNDED_FROM_PAYPAL)) {
            $totalFees += $out->getAmount();
        } else {
            $running = $running + $out->getAmount();
            $latestDate = $out->getDate()->format('d/m/y');

            $data = array(
                $out->getDate()->format('d/m/y'),
                $out->getTransactionId(),
                sprintf('%0.2f', $out->getAmount()),
                sprintf('%0.2f', $running),
                $out->getDescription()
            );
            if (null === $outputFile) {
                print implode(',', $data) . PHP_EOL;
            } else {
                fputcsv($outputHandle, $data);
            }
        }
    }
    if (true === $group) {
        $data = array($latestDate,
            'Paypal Fees Summary',
            sprintf('%0.2f', $totalFees),
            sprintf('%0.2f', $running - abs($totalFees)),
            sprintf('Summarised Paypal Fees for %s to %s',$firstDate,$latestDate)
        );
        if (null === $outputFile) {
            print implode(',', $data) . PHP_EOL;
        } else {
            fputcsv($outputHandle, $data);
        }
    }
    if (null !== $outputFile) {
        fclose($outputHandle);
    }
}

/**
 * Display the help text
 */
function displayHelp()
{
    print 'This script scans a Paypal "Comma Delimited - All Activity" file to reconcile it (taking into account foreign currencies).' . PHP_EOL;
    print 'It will then make a file suitable to reconcile in popular online accounting packages.' . PHP_EOL;
    print 'Parameters are:' . PHP_EOL;
    print ' --input filename (or -i filename)    : The input filename' . PHP_EOL;
    print ' --output filename (or -o filename)   : The output filename' . PHP_EOL;
    print ' --group (or -g)                      : Group all Paypal fees payments into a single entry' . PHP_EOL;
    print ' --currency currency (or -c currency) : What (base) currency are most the entries in (defaults to GBP)' . PHP_EOL;
    print ' --anon (or -a)                       : Anonymise the Paypal file by removing email addresses' . PHP_EOL;
}

/**
 * Merges a short and long settings from the options array. Long settings take precedence
 * @param array $array Our options array
 * @param string $short The label for any short setting (such as "-i")
 * @param string $long The label for any long setting (such as "--input")
 * @return string Any set details
 */
function mergeOptions($array, $short, $long)
{
    $out = null;
    if (true === isset($array[$short])) {
        $out = $array[$short];
    }
    if (true === isset($array[$long])) {
        $out = $array[$long];
    }

    return $out;
}
